using NUnit.Framework;
using Shipping;

namespace ShippingTests
{
    public class Tests
    {
        [TestCase(false, "NextDayAir", 1000.0, ExpectedResult = 0.0)]
        [TestCase(true, "Ground", 1500.1, ExpectedResult = 0.0)]
        [TestCase(true, "Ground", 1500.0, ExpectedResult = 100.0)]
        [TestCase(true, "InStore", 1500.0, ExpectedResult = 50.0)]
        [TestCase(true, "NextDayAir", 1500.0, ExpectedResult = 250.0)]
        [TestCase(true, "SecondDayAir", 1500.0, ExpectedResult = 125.0)]
        [TestCase(true, "RandomUnknownType", 1500.0, ExpectedResult = 0.0)]
        public double TestShippingCosts(bool calculateShippingCosts, string typeOfShippingCosts, double totalPrice)
        {
            // Given
            var subject = new ClassAssignmentAvans();
            // When
            var shippingCosts = subject.ShippingCosts(calculateShippingCosts, typeOfShippingCosts, totalPrice);
            // Then - handled by nUnit
            return shippingCosts;
        }
    }
}