[![coverage](https://cilph.gitlab.io/proftaak-idh12/coverage/badge_branchcoverage.svg "Branch Coverage")](https://cilph.gitlab.io/proftaak-idh12/coverage/index.htm)

[API Testing Logs](https://cilph.gitlab.io/proftaak-idh12/api-report.txt)

GUI Testing Logs - TODO

Performance Testing Logs - Not part of the CI chain as of yet, but a PDF report is available under the related subfolder.