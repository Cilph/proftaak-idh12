import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

random = new Random()

// Why in the Lord's name does this index start at 1?
def electronicsProduct = product(electronics, null, random.nextInt(8) + 2)

def apparelProduct = product(apparel, null, random.nextInt(8) + 2)

def expectedPriceTotal = (electronicsProduct.price * electronicsProduct.quantity) + (apparelProduct.price * apparelProduct.quantity)

WebUI.callTestCase(findTestCase('Modules/Login'), [('email') : email, ('password') : password], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Modules/Empty Shopping Cart'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Modules/Add Product To Shopping Cart'), [('productTitle') : electronicsProduct.id, ('productQuantity') : electronicsProduct.quantity], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Modules/Add Product To Shopping Cart'), [('productTitle') : apparelProduct.id, ('productQuantity') : apparelProduct.quantity], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Modules/Verify Shopping Cart Price'), [('totalPrice') : expectedPriceTotal], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Modules/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

def product(def testdata, def idx, def quantity) {
    if (idx == null) {
        idx = (random.nextInt(testdata.getRowNumbers()) + 1)
    }
    
    return [('id') : testdata.getObjectValue('id', idx), ('price') : Double.parseDouble(testdata.getObjectValue('price', 
                idx)), ('quantity') : quantity]
}

