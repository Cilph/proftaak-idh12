<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>button_update</description>
   <name>button_update</name>
   <tag></tag>
   <elementGuidId>5f7d036f-a6d7-4149-a51a-70b94f955d3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div.master-wrapper-page > div.master-wrapper-content > div.master-wrapper-main > div > div > div.page-body > div > form > div.buttons > div > input.button-2.update-cart-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
